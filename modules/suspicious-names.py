#!/usr/bin/env python3

import git
import inspect
import os
import re
import sys

localmodule = os.path.realpath(
    os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())), '..'))
if localmodule not in sys.path:
    sys.path.insert(0, localmodule)
from issuebot import IssuebotModule


class SuspiciousNames(IssuebotModule):

    bad_chars_pattern = re.compile(r'[^a-zA-Z0-9.,()/+_#@-]')

    def main(self):
        print('Scanning git tag/branch names')
        try:
            repo = git.repo.Repo(self.source_dir)
        except git.exc.InvalidGitRepositoryError as e:
            print(e)
            return
        report = ''
        branches = []
        tags = []
        for branch in repo.branches:
            suspicious = set()
            for c in self.bad_chars_pattern.findall(branch.name):
                suspicious.add(c)
            if suspicious:
                branches.append([str(branch), ''.join(sorted(suspicious))])
        for tag in repo.tags:
            suspicious = set()
            for c in self.bad_chars_pattern.findall(tag.name):
                suspicious.add(c)
            if suspicious:
                tags.append([str(tag), ''.join(sorted(suspicious))])
        if branches or tags:
            self.add_label('suspicious-names')
            report = """<h3>Suspicious tag/branch names</h3>
            <p>Git tag and branch names should only use ASCII in order to avoid accidential or malicious confusion between characters like B, ß, and β.  Other characters can also be used to build exploits.</p>
""" + report
        if branches:
            report += '<details open="true"><summary>branches</summary><ul>'
            for branch, suspicious in sorted(branches):
                report += '<li><code>{}</code> --> ({})</li>'.format(branch, suspicious)
            report += '</ul></details>'
        if tags:
            report += '<details open="true"><summary>tags</summary><ul>'
            for tag, suspicious in sorted(tags):
                report += '<li><code>{}</code> --> ({})</li>'.format(tag, suspicious)
            report += '</ul></details>'
        self.reply['report'] = report
        self.reply['reportData'] = {self.__class__.__name__: {'branches': branches, 'tags': tags}}
        self.write_json()


if __name__ == "__main__":
    SuspiciousNames().main()
