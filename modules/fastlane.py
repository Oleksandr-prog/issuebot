#!/usr/bin/env python3

import inspect
import os
import sys
from fdroidserver import common, metadata, update

localmodule = os.path.realpath(
    os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())), '..'))
if localmodule not in sys.path:
    sys.path.insert(0, localmodule)
from issuebot import IssuebotModule


class Fastlane(IssuebotModule):

    def main(self):
        config = dict()
        common.fill_config_defaults(config)
        update.config = config
        apps = {self.application_id: metadata.App()}
        update.insert_localized_app_metadata(apps)
        if 'localized' in apps[self.application_id]:
            self.reply['labels'].add('fastlane')
            self.reply['reportData'][self.application_id] = apps[self.application_id]
        else:
            apps[self.application_id]['localized'] = dict()  # placeholder

        report = ''
        for locale, v in apps[self.application_id]['localized'].items():
            report += '<details><summary>{locale}</summary>'.format(locale=locale)
            if v.get('name'):
                report += '<b>{}</b>'.format(v['name'])
            if v.get('summary') and v.get('description'):
                report += ('<details><summary>{s}</summary><br /><p>{d}</p></details>'
                           .format(s=v['summary'], d=v['description']))
            report += '</details>'
        if report:
            self.reply['report'] = (
                '<h3>Fastlane</h3><details><summary>by locale</summary>'
                + report + '</details>'
            )
        self.write_json()


if __name__ == "__main__":
    Fastlane().main()
