#!/usr/bin/env python3

import github
import gitlab
import glob
import json
import io
import issuebot
import os
import re
import shutil
import subprocess
import sys
import yaml
import zipfile

try:
    from yaml import CSafeLoader as SafeLoader
except ImportError:
    from yaml import SafeLoader
from colorama import Fore, Style
from datetime import datetime
from fdroidserver import common, metadata
from textwrap import dedent
from urllib.parse import parse_qs, urlparse


APPLICATIONIDS_FILE = os.path.join(issuebot.ISSUEBOT_API_DIR, 'applicationIds.json')
SOURCEURLS_FILE = os.path.join(issuebot.ISSUEBOT_API_DIR, 'sourceUrls.json')
APPID_REGEX = re.compile(r'metadata/(.+)\.yml')


def download_report_history(project):
    """use gitlab API to fetch entire history of reports

    This gets the artifacts from all the jobs listed for this project,
    unpack public/issuebot from them this will provide a complete,
    available history.

    """

    class Extractor(object):
        def __init__(self):
            self._fd = io.BytesIO()

        def __call__(self, chunk):
            self._fd.write(chunk)

        def extract(self):
            with zipfile.ZipFile(self._fd) as zipfp:
                for zipinfo in zipfp.infolist():
                    if zipinfo.filename.startswith(issuebot.ISSUEBOT_API_DIR) \
                       and zipinfo.file_size > 0:
                        outdir = os.path.dirname(zipinfo.filename)
                        os.makedirs(outdir, exist_ok=True)
                        zipfp.extract(zipinfo)

    global applicationIds_data, sourceUrls_data

    applicationIds_data = dict()
    sourceUrls_data = dict()

    print('----------------------------\nGetting Job History for API\n')
    for pipeline in project.pipelines.list():
        print(pipeline.web_url)
        for pipeline_job in pipeline.jobs.list():
            job = project.jobs.get(pipeline_job.id, lazy=True)
            try:
                target = Extractor()
                job.artifacts(streamed=True, action=target)
                target.extract()
            except gitlab.exceptions.GitlabGetError as e:
                print(Fore.YELLOW
                      + ('WARNING: artifacts for pipeline %d job %d (%s) are %s'
                         % (pipeline.id, pipeline_job.id, pipeline_job.status, str(e)))
                      + Style.RESET_ALL)
            if os.path.exists(APPLICATIONIDS_FILE):
                with open(APPLICATIONIDS_FILE) as fp:
                    data = json.load(fp)
                os.remove(APPLICATIONIDS_FILE)
                for appid, job_list in data.items():
                    if appid not in applicationIds_data:
                        applicationIds_data[appid] = []
                    applicationIds_data[appid] += job_list
            if os.path.exists(SOURCEURLS_FILE):
                with open(SOURCEURLS_FILE) as fp:
                    data = json.load(fp)
                os.remove(SOURCEURLS_FILE)
                for sourceUrl, job_list in data.items():
                    if sourceUrl not in sourceUrls_data:
                        sourceUrls_data[sourceUrl] = []
                    sourceUrls_data[sourceUrl] += job_list

    for job_dir in glob.glob(os.path.join(issuebot.ISSUEBOT_API_DIR, '[0-9][0-9][0-9][0-9]*')):
        if not os.path.isdir(job_dir):
            continue
        job_id = os.path.basename(job_dir)
        for issue_dir in glob.glob(os.path.join(job_dir, '[0-9]*')):
            if not os.path.isdir(issue_dir):
                continue
            issue_id = os.path.basename(issue_dir)
            appid = None
            sourceUrl = None
            module_reports = []
            for module_report in glob.glob(os.path.join(issue_dir, '*.json')):
                module_reports.append(os.path.relpath(module_report, issuebot.ISSUEBOT_API_DIR))
                if appid is None or sourceUrl is None:
                    with open(module_report) as fp:
                        data = json.load(fp)
                    appid = data.get('applicationId')
                    sourceUrl = data.get('sourceUrl')
            if appid:
                append_to_applicationIds_data(job_id, issue_id, appid, module_reports)
            if sourceUrl:
                append_to_sourceUrls_data(job_id, issue_id, sourceUrl, module_reports)

    for appid, job_list in applicationIds_data.items():
        jobs = set()
        new_list = []
        for job in job_list:
            if job['jobId'] not in jobs:
                new_list.append(job)
            jobs.add(job['jobId'])
            applicationIds_data[appid] = sorted(new_list, reverse=True,
                                                key=lambda item: item['jobId'])

    for sourceUrl, job_list in sourceUrls_data.items():
        jobs = set()
        new_list = []
        for job in job_list:
            if job['jobId'] not in jobs:
                new_list.append(job)
            jobs.add(job['jobId'])
            sourceUrls_data[sourceUrl] = sorted(new_list, reverse=True,
                                                key=lambda item: item['jobId'])


def create_dummy_fdroid_repo():
    os.makedirs('build', exist_ok=True)
    os.makedirs('metadata', exist_ok=True)
    os.makedirs('repo', exist_ok=True)
    with open('fdroid-icon.png', 'w') as fp:
        fp.write('')
    with open('config.py', 'w') as fp:
        fp.write(dedent("""
            allow_disabled_algorithms = True
            make_current_version_link = False
            repo_pubkey = "deadbeef"
            androidobservatory = True
        """))
        VIRUSTOTAL_API_KEY = os.getenv('VIRUSTOTAL_API_KEY')
        if VIRUSTOTAL_API_KEY:
            fp.write('virustotal_apikey = "%s"\n' % VIRUSTOTAL_API_KEY)


def get_apk_from_github(url, labels):
    if url.startswith('https://github.com/'):
        token = os.getenv('GITHUB_TOKEN')
        g = github.Github(token)
        repo = g.get_repo('/'.join(url.split('/')[3:5]))
        try:
            release = repo.get_latest_release()
            for asset in release.get_assets():
                if asset.browser_download_url.endswith('.apk'):
                    print('Downloading from %s/releases/latest' % url)
                    issuebot.download_file(asset.browser_download_url)
                    labels.add('in-github-releases')
        except github.GithubException as e:
            print(e)


def get_apk_from_izzysoft(appid, labels):
    url = 'https://apt.izzysoft.de/fdroid/index/apk/' + appid
    r = issuebot.requests_head(url)
    if r.status_code == 200:
        labels.add('in-izzysoft')
        r = issuebot.requests_get(url)
        m = issuebot.IZZYSOFT_PATTERN.search(r.text)
        if m:
            dl_url = 'https://apt.izzysoft.de' + m.group(1)
            print('Downloading', dl_url)
            issuebot.download_file(dl_url)


def get_modules():
    modules = []
    for f in glob.glob(os.path.join(os.path.dirname(__file__), 'modules', '*.*')):
        if f[-1] == '~' or not os.path.isfile(f) \
           or not os.access(f, os.R_OK) or not os.access(f, os.X_OK):
            print('\n===================================================================\n',
                  'Skipping', f)
        else:
            modules.append(f)
    return sorted(modules)


def setup_for_modules():
    if 'CI' not in os.environ or os.getuid() != 0:
        print(Fore.YELLOW + 'WARNING: not running in CI environment as root!' + Style.RESET_ALL)
        return

    cmd = [
        'bash',
        'fdroidserver/buildserver/provision-apt-get-install',
        'http://deb.debian.org/debian',
    ]
    issuebot.run_cli_tool(cmd)

    apt_regex = re.compile(r'''\s*(?:#|//)\s*issuebot_apt_install\s*[:= ]\s*(.*?)\n''')
    pip_regex = re.compile(r'''\s*(?:#|//)\s*issuebot_pip_install\s*[:= ]\s*(.*?)\n''')
    for f in get_modules():
        print('\n===================================================================\n',
              'Setting up', f)
        with open(f) as fp:
            data = fp.read()
        apt = apt_regex.search(data)
        if apt:
            cmd = ['apt-get', 'install', ] + apt.group(1).strip().split()
            issuebot.run_cli_tool(cmd)
        pip = pip_regex.search(data)
        if pip:
            cmd = ['pip3', '--timeout', '100', '--retries', '10', 'install', ]
            cmd += pip.group(1).strip().split()
            issuebot.run_cli_tool(cmd)


def run_per_app(local_issue_id, appid):
    os.environ['ISSUEBOT_CURRENT_ISSUE_ID'] = str(local_issue_id)
    os.environ['ISSUEBOT_CURRENT_APPLICATION_ID'] = appid
    emoji = set()
    labels = set()
    report = ''
    get_apk_from_izzysoft(appid, labels)
    for f in get_modules():
        print('\n===================================================================\n',
              'Running', f)
        try:
            p = subprocess.run([f], timeout=1800)
            if p.returncode == 0:
                with open(os.path.join(issuebot.get_json_dir(), os.path.basename(f) + '.json')) as fp:
                    data = yaml.load(fp, Loader=SafeLoader)
                emoji.update(data['emoji'])
                labels.update(data['labels'])
                report += data['report']
        except (FileNotFoundError, subprocess.TimeoutExpired,
                yaml.scanner.ScannerError, yaml.reader.ReaderError) as e:
            print(e)

    print('\n===================================================================\n',
          'results:')
    print(sorted(emoji))
    print(sorted(labels))
    with open(os.path.join(issuebot.get_json_dir(), appid + 'report.html'), 'w') as fp:
        fp.write('<!DOCTYPE html>\n<html lang="en"><head><title>report</title><meta http-equiv="refresh" content="5"></head><body><h1>{}'
                 .format(appid))
        fp.write('</h1>')
        if emoji:
            fp.write('<h3>emoji</h3><ul>')
            for e in sorted(emoji):
                fp.write('<li>{}</li>'.format(e))
            fp.write('</ul>')
        if labels:
            fp.write('<h3>labels</h3><ul>')
            for label in sorted(labels):
                fp.write('<li>{}</li>'.format(label))
            fp.write('</ul>')
        fp.write(report)
        fp.write('</body></html>')

    return emoji, labels, report


def update_entry_index(name, data):
    """Merge the current data with the published archive

    This waits forever until it can fetch the archive to avoid losing
    data.  If we can't get the archive, then the whole job run should
    be cancelled.  This makes issuebot quit if there are
    network/server issues.  Issuebot will run again on a schedule, so
    its better to abort this job run rather than lose the data archive
    that's stored on GitLab Pages.

    If the URL gives back 404 Not Found, then that means this is the
    first run, and there is no archive.  It seems GitLab Pages might
    also return 503 when it is uninitialized.

    """
    group, project = os.getenv('CI_PROJECT_PATH').split('/')
    url = ('https://{group}.gitlab.io/{project}/issuebot/{name}.json'
           .format(group=group, project=project, name=name))
    archive = None
    while True:
        r = issuebot.requests_get(url)
        if r.status_code == 404 or r.status_code == 503:
            archive = dict()
            break
        elif r.status_code == 200:
            archive = r.json()
            break
        else:
            print('Trying again:', r.status_code, url)
    for appid, entry in data.items():
        if appid in archive:
            archive[appid].insert(0, entry[0])
        else:
            archive[appid] = entry
    output = dict()
    for appid, entries in archive.items():
        jobIds = set()
        for entry in entries:
            if entry['jobId'] not in jobIds:
                if appid not in output:
                    output[appid] = []
                output[appid].append(entry)
            jobIds.add(entry['jobId'])
    os.makedirs(issuebot.ISSUEBOT_API_DIR, exist_ok=True)
    print('WRITING', os.path.join(issuebot.ISSUEBOT_API_DIR, name + '.json'))
    with open(os.path.join(issuebot.ISSUEBOT_API_DIR, name + '.json'), 'w') as fp:
        json.dump(output, fp, indent=2, sort_keys=True)


def append_to_applicationIds_data(job_id, issue_id, appid, datafiles, builds=[], successful_builds=[]):
    global applicationIds_data
    if appid not in applicationIds_data:
        applicationIds_data[appid] = []
    applicationIds_data[appid].append({
        'attemptedBuilds': builds,
        'jobId': job_id,
        'issueId': issue_id,
        'modules': datafiles,
        'successfulBuilds': successful_builds,
    })


def append_to_sourceUrls_data(job_id, issue_id, sourceUrl, datafiles, builds=[], successful_builds=[]):
    global sourceUrls_data
    if sourceUrl not in sourceUrls_data:
        sourceUrls_data[sourceUrl] = []
    sourceUrls_data[sourceUrl].append({
        'attemptedBuilds': builds,
        'jobId': job_id,
        'issueId': issue_id,
        'modules': datafiles,
        'successfulBuilds': successful_builds,
    })


def process_issue(project, issue, appid=None):
    """Run all the steps on an issue"""

    global processed

    print('=================================================================')
    print(issue.get_id(), issue.title)
    print('------------------------------------------------------------------')
    labels = set(issue.labels)
    if 'fdroid-bot' in labels:
        print('Skipping %s (already has fdroid-bot label).' % issue.title)
        return
    processed += 1
    labels.add('fdroid-bot')

    discovered_appids = set()
    gplay_urls = set()
    for m in issuebot.GPLAY_PATTERN.finditer(issue.description):
        gplay_urls.add(m.group(0))
    for url in gplay_urls:
        discovered_appids.add(parse_qs(urlparse(url).query)['id'][0])

    for appid in issuebot.APPLICATION_ID_PATTERN.findall(issue.description):
        discovered_appids.add(appid)

    git_urls = issuebot.parse_git_urls_from_description(issue.description)
    for git_url in git_urls:
        labels.add('git-url')
        issuebot.run_cli_tool(['fdroid', 'import', '--url', git_url, '--omit-disable'])
        get_apk_from_github(git_url, labels)

    common.config = {'accepted_formats': ['yml']}
    metadata.warnings_action = None
    apps = metadata.read_metadata()
    if len(git_urls) > 1:
        labels.add('too-many-git-urls')
    if len(discovered_appids) > 1:
        labels.add('too-many-ApplicationIds')

    for id, app in apps.items():
        if app.get('SourceCode') in git_urls \
           and (len(git_urls) == 1 or appid in discovered_appids):
            appid = id
            os.environ['ISSUEBOT_CURRENT_SOURCE_URL'] = app['SourceCode']
            break

    process_issue_or_merge_request(project, issue, appid, git_urls, labels)


def process_issue_or_merge_request(project, issue, appid=None, git_urls=[], labels=set(), builds=[]):
    """Run all the steps on an issue or merge request

    This will try to make APKs available, either by building the
    entries in the 'builds' parameter, or by rsyncing from an official
    mirror.  This is useful for testing things like Fastlane support.

    The name "issue" is currently stuck because this was first
    developed only around issues.  Now, an "issue" can be an instance
    of either gitlab.v4.objects.ProjectIssue or
    gitlab.v4.objects.ProjectMergeRequest.  Duck typing makes it work,
    since the basic API is the same between them.  One caveat is that
    they are treated differently in regards to permissions.  For
    example, GitLab "Reporter" status gives access to label issues,
    but not merge requests.

    """
    datafiles = None
    issue_id = int(issue.get_id())
    jobId = os.getenv('CI_JOB_ID')
    if not jobId:
        os.environ['CI_JOB_ID'] = datetime.utcnow().strftime('%s')
    successful_builds = []

    labels.update(issue.labels)

    if appid:
        if isinstance(issue, gitlab.v4.objects.ProjectMergeRequest):
            common.config = {'accepted_formats': ['yml']}
            metadata.warnings_action = None
            apps = metadata.read_metadata()
            os.environ['ISSUEBOT_CURRENT_SOURCE_URL'] = apps[appid]['SourceCode']

        if builds:
            for build in builds:
                # fdroid build --on-server expects sudo is installed, then uninstalls it
                p = issuebot.run_cli_tool(['apt-get', 'install', 'sudo'])
                print(p.stdout.decode())
                p = issuebot.run_cli_tool(['fdroid', 'fetchsrclibs', '--verbose', build],
                                          timeout=3600)
                print(p.stdout.decode())
                p = issuebot.run_cli_tool(['fdroid', 'build',
                                           '--verbose', '--on-server', '--no-tarball', build],
                                          timeout=3600)
                print(p.stdout.decode())
            for f in glob.glob('unsigned/%s_*.apk' % appid):
                successful_builds.append(f)
        else:
            # run fdroid scanner as a way to checkout the source code
            p = issuebot.run_cli_tool(['fdroid', 'scanner', '--quiet', '-Wignore'],
                                      timeout=600)
            print(p.stdout.decode())

        _emoji, _labels, report = run_per_app(issue_id, appid)
        metadata_file = 'metadata/%s.yml' % appid
        datafiles = [f[16:] for f in glob.glob(
            os.path.join(issuebot.ISSUEBOT_API_DIR, str(jobId), str(issue_id), '*.json'))]
        labels.update(_labels)
        try:
            project.labels.create({'name': appid, 'color': '#eee'})
        except gitlab.exceptions.GitlabCreateError as e:
            print(appid, e)
        labels.add(appid)
        append_to_applicationIds_data(jobId, issue_id, appid, datafiles, builds, successful_builds)

        if successful_builds:
            if len(successful_builds) == len(builds):
                labels.add('builds')
                report += '\n<h1><tt>%s</tt> Builds!</h1><ul><li>' % appid
            else:
                report += '\n<h1><tt>%s</tt>Some builds succeeded</h1><ul><li>' % appid
            report += '</li><li>'.join(successful_builds)
            report += '</li></ul>\n\n'

        if os.getenv('CI_PROJECT_NAME') == 'rfp':
            if successful_builds:
                report += ('This metadata file builds (_metadata/%s.yml_):\n```yaml\n'
                           % appid)
                with open(metadata_file) as fp:
                    report += fp.read()
                report += '\n```\n\n'

            fdroiddata_url = (
                'https://gitlab.com/fdroid/fdroiddata/-/commits/master/metadata/%s.yml'
                % appid
            )
            r = issuebot.requests_head(fdroiddata_url)
            if r.status_code == 200:
                report += ('Closing issue, <tt>%s</tt> already in included in fdroiddata:\n%s'
                           % (appid, fdroiddata_url))
                labels.add('in-fdroiddata')
                issue.state_event = 'close'

        if report:
            if os.getenv('FROM_CI_JOB_ID'):
                # triggered by trigger-issuebot job from the issue or fork
                header = ('<p><small>triggered by <a href="%s/-/jobs/%s"><tt>%s</tt></a></small></p>'
                          % (os.getenv('FROM_CI_PROJECT_URL'),
                             os.getenv('FROM_CI_JOB_ID'),
                             os.getenv('FROM_CI_COMMIT_SHA')))
            elif os.getenv('FROM_CI_COMMIT_SHA'):
                # triggered by the scheduled job in fdroid/fdroiddata
                header = ('<p><small>triggered by <tt>%s</tt></small></p>'
                          % (os.getenv('FROM_CI_COMMIT_SHA')))
            else:
                header = ''
            report = header + report
            report += ('<p><small>generated by <a href="%s/-/jobs/%s">GitLab CI Job #%s</a></small></p>'
                       % (os.getenv('CI_PROJECT_URL'), jobId, jobId))
            with open('public/%s-%s-%s-report.txt' % (jobId, issue_id, appid), 'w') as fp:
                fp.write(report)
            issue.notes.create({'body': report[:1000000]})

        if not git_urls and os.path.isfile(metadata_file):
            try:
                with open(metadata_file) as fp:
                    data = yaml.load(fp, Loader=SafeLoader)
                if data and data.get('SourceCode'):
                    git_urls = [data['SourceCode']]
            except (FileNotFoundError, yaml.scanner.ScannerError, yaml.reader.ReaderError) as e:
                print(Fore.YELLOW + 'WARNING: ' + str(e) + Style.RESET_ALL)

    if labels:
        issue.labels = sorted(labels)

    try:
        issue.save()
    except gitlab.exceptions.GitlabUpdateError as e:
        if isinstance(issue, gitlab.v4.objects.ProjectMergeRequest):
            print(Fore.YELLOW + 'WARNING: "' + str(e)
                  + '": issuebot probably failed to post labels since Developer '
                  + 'status is required to post labels to merge requests:\n'
                  + Style.RESET_ALL)
        else:
            raise(e)

    for git_url in git_urls:
        append_to_sourceUrls_data(jobId, issue_id, git_url, builds, successful_builds)

    if issue.description:
        for url in re.findall(r'https?://[^ :]+\.apk', issue.description, flags=re.IGNORECASE):
            issuebot.download_file(url)

    p = issuebot.run_cli_tool(['fdroid', 'update',
                               '--rename-apks', '--create-metadata', '--nosign'])
    print(p.stdout.decode())


def get_valid_merge_request(project):
    """Get a merge_request instance if issuebot was triggered by a merge request

    If ci_pipeline_id is an int, we're running in a merge request that
    triggered us.  Then fetch the merge request info, see if it
    changes a metadata file, and if so, then set up the local repo for
    working with.  This GitLab CI Job must have been started by a
    trigger script for it to receive the FROM_CI_* variables.

    """
    try:
        ci_merge_request_iid = int(os.getenv('FROM_CI_MERGE_REQUEST_IID'))
    except (TypeError, ValueError) as e:
        print(Fore.RED + 'ERROR: FROM_CI_MERGE_REQUEST_IID env var ' + str(e) + Style.RESET_ALL)
        return
    merge_request = None
    try:
        merge_request = project.mergerequests.get(ci_merge_request_iid)
        if merge_request is not None:
            return merge_request
    except gitlab.exceptions.GitlabGetError as e:
        print(Fore.YELLOW + 'WARNING: ' + str(e) + Style.RESET_ALL)
    try:
        ci_pipeline_id = int(os.getenv('FROM_CI_PIPELINE_ID'))
    except (TypeError, ValueError) as e:
        print(Fore.RED + 'ERROR: FROM_CI_PIPELINE_ID env var ' + str(e) + Style.RESET_ALL)
        return
    for mr in project.mergerequests.list(lazy=True, per_page=250,
                                         state='opened', order_by='updated_at'):
        for pipeline in mr.pipelines():
            if ci_pipeline_id == pipeline['id']:
                merge_request = mr
                break
        if merge_request:
            break
    return merge_request


def get_builds_in_merge_request(gl, merge_request):
    """Compare a merge request commits to master to find the changes

    This works just like tools/find-changed-builds.py in fdroiddata's
    `fdroid build` job, since that is how gitlab-ci generally
    operates.  Here, issuebot starts on the older commit, so the `git
    checkout` command needs to be run before this function.  In
    fdroiddata's `fdroid build` job, the script runs starting at the
    newer commit.

    """
    # find HEAD commit of the merge request's fork
    commit_id = merge_request.sha
    if not commit_id:
        commit_id = os.getenv('FROM_CI_COMMIT_SHA')
    if not commit_id:
        print(Fore.RED + 'ERROR: invalid FROM_CI_COMMIT_SHA: "{}"'.format(str(commit_id)) + Style.RESET_ALL)
        return [], []
    os.chdir(os.getenv('CI_PROJECT_DIR'))
    source_project = gl.projects.get(merge_request.source_project_id)
    source_project_url = source_project.http_url_to_repo
    if not source_project_url:
        source_project_url = os.getenv('FROM_CI_PROJECT_URL')
    if not source_project_url or not commit_id:
        print(Fore.YELLOW
              + 'WARNING: cannot process merge request builds without source URL and commit ID'
              + Style.RESET_ALL)
        return [], []
    issuebot.run_cli_tool(['git', 'fetch', source_project_url, commit_id])
    issuebot.run_cli_tool(['git', 'reset', '--hard', commit_id])
    upstream_commit_id = os.getenv('CI_COMMIT_SHA')
    output = subprocess.check_output(
        ['git', 'diff', '--name-only', '--diff-filter=d',
         '%s...%s' % (upstream_commit_id, commit_id)]
    )
    changed_appids = set()
    for i in output.decode().split():
        m = APPID_REGEX.match(i)
        if m:
            changed_appids.add(m.group(1))
    builds = dict()
    for appid in changed_appids:
        builds[appid] = []
        metadata_file = 'metadata/%s.yml' % appid
        diff = subprocess.check_output(
            ['git', 'diff', '--no-color', '--diff-filter=d',
             '%s...%s' % (upstream_commit_id, commit_id),
             '--', metadata_file]
        )

        with open(metadata_file) as fp:
            current = yaml.safe_load(fp)
        cmd = 'git apply --reverse'
        p = subprocess.run(cmd.split(' '), input=diff, stdout=subprocess.DEVNULL)
        if p.returncode:
            print(Fore.RED + ('ERROR: %s: %d' % (cmd, p.returncode)) + Style.RESET_ALL,
                  file=sys.stderr)
            continue
        to_build = []
        if os.path.exists(metadata_file):
            with open(metadata_file) as fp:
                previous = yaml.safe_load(fp)
            cmd = 'git apply'
            p = subprocess.run(cmd.split(' '), input=diff, stdout=subprocess.DEVNULL)
            if p.returncode:
                print(Fore.RED + ('ERROR: %s: %d' % (cmd, p.returncode)) + Style.RESET_ALL,
                      file=sys.stderr)
                continue

            previous_builds = dict()
            for build in previous['Builds']:
                previous_builds[build['versionCode']] = build

            for build in current['Builds']:
                vc = build['versionCode']
                if vc not in previous_builds:
                    to_build.append(vc)
                    continue
                if build != previous_builds[vc]:
                    to_build.append(vc)
        else:
            # this is a brand new metadata file
            cmd = 'git checkout -- ' + metadata_file
            p = subprocess.run(cmd.split(' '), stdout=subprocess.DEVNULL)
            if p.returncode:
                print(
                    Fore.RED + ('ERROR: %s: %d' % (cmd, p.returncode)) + Style.RESET_ALL,
                    file=sys.stderr,
                )
                sys.exit(p.returncode)
            with open(metadata_file) as fp:
                data = yaml.safe_load(fp)
            for build in data['Builds']:
                to_build.append(build['versionCode'])

        for vc in to_build:
            builds[appid].append('%s:%d' % (appid, vc))

    return changed_appids, builds


def main():
    global processed, applicationIds_data, sourceUrls_data
    processed = 0

    private_token = os.getenv('PERSONAL_ACCESS_TOKEN')
    if not private_token:
        print(Fore.RED + 'ERROR: GitLab Token not found in PERSONAL_ACCESS_TOKEN!' + Style.RESET_ALL)
        sys.exit(1)
    gl = gitlab.Gitlab('https://gitlab.com', api_version=4,
                       private_token=private_token)
    project = gl.projects.get(os.getenv('CI_PROJECT_PATH'), lazy=True)

    download_report_history(project)

    merge_request = get_valid_merge_request(project)
    appid = None
    builds = []
    to_process = None

    if merge_request:
        appids, builds = get_builds_in_merge_request(gl, merge_request)
        print('This merge request changed these apps:')
        print('\n'.join(appids))
        print('and these specific build entries:')
        for args in builds.values():
            for arg in args:
                print(arg)
        # remove extra metadata/ files from processing and artifacts
        for f in glob.glob('metadata/*.*'):
            if os.path.isdir(f) and os.path.basename(f) not in appids:
                shutil.rmtree(f, ignore_errors=True)
            elif os.path.isfile(f) and os.path.basename(f)[:-4] not in appids:
                os.remove(f)
        if len(appids) > 0:
            to_process = merge_request
    else:
        to_process = project.issues.list(state='opened', order_by='updated_at', per_page=250)

    if to_process:
        create_dummy_fdroid_repo()
        setup_for_modules()

    if isinstance(to_process, gitlab.v4.objects.ProjectMergeRequest):
        for appid in appids:
            process_issue_or_merge_request(project, to_process, appid, builds=builds[appid])
    elif to_process is not None:
        for issue in to_process:
            if processed > 5:
                print('Processed %d items, quitting.' % processed)
                break
            process_issue(project, issue)

    update_entry_index('applicationIds', applicationIds_data)
    update_entry_index('sourceUrls', sourceUrls_data)


if __name__ == "__main__":
    main()
